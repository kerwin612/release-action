package main

import "testing"

func Do_getFiles(t *testing.T, parentDir, keys string, no int) {
    files, err := getFiles(parentDir, keys)
    if err != nil {
        t.Fatalf("getFiles failed in %s by %s, %v", parentDir, keys, err)
    }
    if len(files) != no {
        t.Fatalf("getFiles in %s by %s, expected %d, but got %d", parentDir, keys, no, len(files))
    }
}

func Test_getFiles(t *testing.T) {
    Do_getFiles(t, ".", "*.mod", 1)
    Do_getFiles(t, ".", "*.mod *.sum", 2)
    Do_getFiles(t, ".", "*.mod\n*.sum", 2)
}
